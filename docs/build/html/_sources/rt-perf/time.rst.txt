.. _time:

========================
Performance of HYDRA
========================

In this section, we will go through the real-time performance
of HYDRA

Serial Mode
------------

	- Momentum theory + statistical mode: 350 cases/second
	- Momentum theory + FEA for airframe + physics based rotor weight: 50 cases/second
	- BEMT + FEA for airframe + physics based rotor weight: 2-4 cases/second

Parallel Mode
-------------
	
	- ......COMING SOON ......