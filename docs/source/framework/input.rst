.. _framework:

=================================
Code Structure of HYDRA
=================================

Basically, this section will deal how the code is organised in HYDRA.

  - Input file (yaml)
  	- to be added
  - Python Class - Hydra interface
  	- to be added
  - BEMT
  	- to be added
  	- to be added
  - FEA
  	- to be added

-----------------
Input
-----------------

	The input is read in a yaml file.....
	units.....

-----------------
Class Hierarchy
-----------------

	Aircraft class.......
	subclasses......
	3 stages defined below:

-----------------
Stage 0
-----------------

	Stage 0

-----------------
Stage 1
-----------------

	Stage 1

-----------------
Stage 3
-----------------

	Stage 3